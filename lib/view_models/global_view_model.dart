import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:phone_validate_app/constants/cust_color.dart';

class Global extends ChangeNotifier {
  static Global instance = Global();
  int currentLang;
  int theme;
  List<String> phoneRecords = [];

  updateTheme(int theme) {
    Global.instance.theme = theme;

    CustColor.updateColorByTheme(theme);
    notifyListeners();
  }
}

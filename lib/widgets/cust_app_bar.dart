import 'package:phone_validate_app/constants/cust_color.dart';
import 'package:phone_validate_app/constants/cust_font_size.dart';
import 'package:phone_validate_app/services/navigation_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'cust_text.dart';

class CustAppBar extends PreferredSize {
  final BuildContext context;
  final String screenName;
  final String title;
  final List<Widget> actions;
  const CustAppBar({this.context, this.screenName, this.title, this.actions});

  @override
  Size get preferredSize => Size.fromHeight(50.0);

  @override
  Widget get child => AppBar(
        leading: NavigationService.instance.getRouteStack().indexOf(screenName) > 0
            ? IconButton(
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: CustColor.primaryTextColor,
                ),
                onPressed: () {
                  NavigationService.instance.pop(context);
                })
            : Container(),
        elevation: 1,
        title: CustText(
          title ?? '',
          color: CustColor.primaryTextColor,
          fontWeight: FontWeight.bold,
          size: CustFontSize.xxLarge,
        ),
        backgroundColor: CustColor.appBarColor,
        actions: actions,
        // brightness: Brightness.light,
      );
}

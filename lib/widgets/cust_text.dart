import 'package:phone_validate_app/constants/cust_color.dart';
import 'package:phone_validate_app/constants/cust_font_size.dart';
import 'package:flutter/material.dart';

class CustText extends StatelessWidget {
  final String text;
  final Color color;
  final double size;
  final FontWeight fontWeight;
  final TextAlign align;
  final int maxLines;
  final bool underlined;

  const CustText(
    this.text, {
    Key key,
    this.color,
    this.size = CustFontSize.medium,
    this.fontWeight = FontWeight.normal,
    this.align = TextAlign.start,
    this.maxLines = 2,
    this.underlined = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      this.text,
      textScaleFactor: 1,
      softWrap: false,
      overflow: TextOverflow.ellipsis,
      maxLines: maxLines,
      textAlign: align,
      strutStyle: StrutStyle(
        fontSize: size,
        height: 1.5,
        forceStrutHeight: true,
      ),
      style: TextStyle(
        fontSize: this.size,
        color: this.color ?? CustColor.primaryTextColor,
        fontWeight: this.fontWeight,
        decoration: underlined ? TextDecoration.underline : null,
      ),
    );
  }
}

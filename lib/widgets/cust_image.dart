import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:phone_validate_app/constants/cust_color.dart';

class CustImage extends StatelessWidget {
  final String imageUrl;
  final BoxFit resize;
  final double width;
  final double height;
  final String placeholderAsset;
  final Color color;
  CustImage(this.imageUrl,
      {this.resize = BoxFit.contain, this.height, this.color, this.width, this.placeholderAsset});

  @override
  Widget build(BuildContext context) {
    if (imageUrl == null || imageUrl.isEmpty)
      return Placeholder();
    else {
      return CachedNetworkImage(
        color: color,
        imageUrl: imageUrl,
        fit: resize,
        width: width,
        height: height,
        placeholder: (context, url) {
          return Center(
            child: LayoutBuilder(
              builder: (context, constraints) => Container(
                width: min(50, constraints.maxHeight),
                height: min(50, constraints.maxHeight),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(CustColor.primaryColor),
                ),
              ),
            ),
          );
        },
        errorWidget: (context, url, error) {
          return Placeholder();
        },
      );
    }
  }

  Placeholder() {
    return Container(
      width: width,
      height: height,
      child: placeholderAsset != null
          ? Image.asset(placeholderAsset)
          : Icon(
              Icons.image,
              color: Colors.black38,
            ),
    );
  }
}

import 'package:phone_validate_app/constants/cust_font_size.dart';
import 'package:flutter/material.dart';

import '../constants/cust_color.dart';
import 'cust_text.dart';

class CustButton extends StatefulWidget {
  final String text;
  final Widget image;
  final Color bgColor;
  final Color fontColor;
  final Color borderColor;
  final Function(CustButtonState state) onTap;
  final double fontSize;
  final double height;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final bool disabled;

  CustButton(
    this.text, {
    Key key,
    this.image,
    this.onTap,
    this.bgColor,
    this.fontColor,
    this.fontSize = CustFontSize.small,
    this.height = 40,
    this.borderColor,
    this.padding,
    this.margin,
    this.disabled,
  }) : super(key: key);

  final CustButtonState state = CustButtonState();

  @override
  CustButtonState createState() => state;
}

class CustButtonState extends State<CustButton> {
  bool isLoading = false;

  updateLoading(bool isLoading) {
    setState(() {
      this.isLoading = isLoading;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: widget.disabled == true ? 0.5 : 1,
      child: Container(
        margin: widget.margin,
        height: widget.height,
        decoration: BoxDecoration(
          border: Border.all(width: 1.0, color: widget.borderColor ?? CustColor.primaryColor),
          color: widget.bgColor ?? CustColor.primaryColor,
          borderRadius: BorderRadius.circular(6),
        ),
        padding: widget.padding ?? EdgeInsets.symmetric(horizontal: 15),
        child: InkWell(
          onTap: widget.disabled == true
              ? null
              : () {
                  if (!isLoading && widget.onTap != null) widget.onTap(this);
                },
          borderRadius: BorderRadius.circular(6),
          child: Center(
            child: isLoading
                ? SizedBox(
                    height: 15.0,
                    width: 15.0,
                    child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(CustColor.primaryColor)),
                  )
                : Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      if (widget.image != null)
                        Padding(
                          child: widget.image,
                          padding: EdgeInsets.only(right: 10),
                        ),
                      ConstrainedBox(
                        constraints: BoxConstraints(maxWidth: 200),
                        child: CustText(widget.text,
                            color: widget.fontColor ?? CustColor.primaryOppositeTextColor,
                            size: widget.fontSize,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}

import 'package:devicelocale/devicelocale.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:phone_validate_app/constants/cust_color.dart';
import 'package:phone_validate_app/constants/cust_font_size.dart';
import 'package:phone_validate_app/generated/Localized.dart';
import 'package:phone_validate_app/models/region_info.dart';
import 'package:phone_validate_app/routes/route_names.dart';
import 'package:phone_validate_app/services/http/phone_api.dart';
import 'package:phone_validate_app/services/navigation_service.dart';
import 'package:phone_validate_app/view_models/global_view_model.dart';
import 'package:phone_validate_app/views/main/notifiers/phone_validator.dart';
import 'package:phone_validate_app/views/main/widgets/phone_field.dart';
import 'package:phone_validate_app/widgets/cust_app_bar.dart';
import 'package:phone_validate_app/widgets/cust_button.dart';
import 'package:phone_validate_app/widgets/cust_text.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../base/base_screen.dart';

class MainViewParam {}

class MainView extends BaseScreen<MainViewParam> {
  MainView(MainViewParam params) : super(params);

  @override
  _MainViewState createState() => _MainViewState();
}

class _MainViewState extends BaseState<MainView> {
  final PhoneValidator phoneValidator = PhoneValidator();
  final FocusNode areaFocusNode = FocusNode();
  final FocusNode phoneFocusNode = FocusNode();
  final TextEditingController areaCodeController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();

  @override
  void initState() {
    super.initState();
    Future.wait([Devicelocale.currentAsLocale, PhoneApi().getRegions()]).then((ret) {
      Locale locale = ret[0];
      List<RegionInfo> regions = ret[1];

      phoneValidator.regions = regions;
      phoneValidator.currentRegion =
          regions.firstWhere((element) => element.code == locale.countryCode, orElse: () => null);
      phoneValidator.area = phoneValidator.currentRegion?.prefix;
      areaCodeController.text = phoneValidator.area;
      setState(() {});
    });
  }

  @override
  Widget body() {
    return outer(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(20),
            child: PhoneField(
              labelText: translate(Localized.Phone_No),
              areaFocusNode: areaFocusNode,
              phoneFocusNode: phoneFocusNode,
              areaCodeController: areaCodeController,
              phoneController: phoneController,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: CustButton(
              translate(Localized.Validate_Phone),
              onTap: (_) async {
                onSubmitClicked();
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget bigBody() {
    return outer(
      child: Column(
        children: [
          Row(
            children: [
              Flexible(
                flex: 2,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(20, 20, 0, 20),
                  child: PhoneField(
                    labelText: translate(Localized.Phone_No),
                    areaFocusNode: areaFocusNode,
                    phoneFocusNode: phoneFocusNode,
                    areaCodeController: areaCodeController,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(20),
                child: IconButton(
                  icon: Icon(Icons.arrow_forward, color: CustColor.primaryColor),
                  onPressed: () async {
                    onSubmitClicked();
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget outer({Widget child}) {
    return Scaffold(
      appBar: CustAppBar(
        context: context,
        screenName: screenName(),
        title: translate(Localized.Home),
        actions: [
          IconButton(
            icon: Icon(
              Icons.settings,
              color: CustColor.primaryTextColor,
            ),
            onPressed: () {
              NavigationService.instance.pushNamed(context, RouteName.SettingRoute);
            },
          ),
        ],
      ),
      backgroundColor: backgroundColor(),
      body: GestureDetector(
        onTap: () {
          areaFocusNode.unfocus();
          phoneFocusNode.unfocus();
        },
        behavior: HitTestBehavior.opaque,
        child: ChangeNotifierProvider.value(value: phoneValidator, child: child),
      ),
    );
  }

  onSubmitClicked() async {
    bool success = await phoneValidator.validatePhone();
    if (!success) {
      Alert(
        context: context,
        title: translate(Localized.Error),
        desc: translate(Localized.Invalid_Format),
        buttons: [
          DialogButton(
            color: CustColor.primaryColor,
            child: CustText(
              translate(Localized.Ok),
              color: CustColor.primaryOppositeTextColor,
              size: CustFontSize.large,
            ),
            onPressed: () => Navigator.pop(context),
            width: 120,
          )
        ],
      ).show();
    } else {
      var phone = phoneValidator.getFormattedPhone();
      if (Global.instance.phoneRecords.contains(phone)) {
        Global.instance.phoneRecords.remove(phone);
      }
      Global.instance.phoneRecords.add(phone);
      areaFocusNode.unfocus();
      phoneFocusNode.unfocus();
      NavigationService.instance.pushNamed(context, RouteName.ResultRoute);
    }
  }

  @override
  String screenName() => RouteName.MainRoute;
}

import 'package:flutter/material.dart';
import 'package:phone_number/phone_number.dart' as PhoneNumber;
import 'package:phone_validate_app/models/region_info.dart';

class PhoneValidator extends ChangeNotifier {
  List<RegionInfo> regions = [];
  RegionInfo currentRegion;

  String area;
  String phone;
  RegionInfo region;
  bool valid;

  getFormattedPhone() {
    return '$area-$phone';
  }

  List<RegionInfo> filter(String text) {
    if (text == null || text.isEmpty) {
      return regions;
    }
    return regions.where((e) => e.prefix.toString().contains(text)).toList();
  }

  Future<bool> validatePhone() async {
    try {
      await PhoneNumber.PhoneNumberUtil().parse(phone, regionCode: region?.code ?? area);
      return true;
    } catch (e) {
      return false;
    }
  }

  updateArea(String area) {
    this.region =
        regions.firstWhere((element) => element.prefix.toString().contains(area), orElse: () => null);
    this.area = area;
    notifyListeners();
  }

  updatePhone(String phone) {
    this.phone = phone;
    notifyListeners();
  }
}

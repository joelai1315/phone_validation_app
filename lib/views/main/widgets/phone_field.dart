import 'dart:math';

import 'package:flutter/material.dart';
import 'package:phone_validate_app/constants/cust_color.dart';
import 'package:phone_validate_app/models/region_info.dart';
import 'package:phone_validate_app/views/main/notifiers/phone_validator.dart';
import 'package:phone_validate_app/widgets/cust_text.dart';
import 'package:provider/provider.dart';

import 'cust_text_form_field.dart';

class PhoneField extends StatefulWidget {
  final String labelText;
  final List<FormFieldValidator> validators;
  final FocusNode areaFocusNode;
  final FocusNode phoneFocusNode;
  final TextEditingController areaCodeController;
  final TextEditingController phoneController;

  PhoneField({
    Key key,
    this.labelText,
    this.validators,
    this.areaFocusNode,
    this.phoneFocusNode,
    this.areaCodeController,
    this.phoneController,
  }) : super(key: key);

  @override
  _PhoneFieldState createState() => _PhoneFieldState();
}

class _PhoneFieldState extends State<PhoneField> {
  PhoneValidator validator;
  OverlayEntry _overlayEntry;

  @override
  void initState() {
    super.initState();
    widget.areaFocusNode.addListener(() {
      if (widget.areaFocusNode.hasFocus) {
        this._overlayEntry = this._createOverlayEntry();
        validator.updateArea(widget.areaCodeController.text);
        Overlay.of(context).insert(this._overlayEntry);
      } else {
        this._overlayEntry.remove();
      }
    });
  }

  OverlayEntry _createOverlayEntry() {
    RenderBox renderBox = context.findRenderObject();
    var size = renderBox.size;
    var offset = renderBox.localToGlobal(Offset.zero);

    return OverlayEntry(
      builder: (context) => Positioned(
        left: offset.dx,
        top: offset.dy + size.height + 5.0,
        width: size.width,
        child: Material(
          elevation: 4.0,
          child: ChangeNotifierProvider.value(
            value: validator,
            child: Consumer(
              builder: (context, PhoneValidator provider, _) {
                if (validator.area != null) {
                  List<RegionInfo> filteredAreaCodes = validator.filter(validator.area);
                  return Container(
                    height: min(5, filteredAreaCodes.length) * 48.0,
                    child: ListView.builder(
                      padding: EdgeInsets.zero,
                      itemBuilder: (context, index) {
                        RegionInfo region = filteredAreaCodes[index];
                        return _getListTile(region.prefix.toString(), region.code);
                      },
                      itemCount: filteredAreaCodes.length,
                    ),
                  );
                } else {
                  return Container();
                }
              },
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    validator = Provider.of<PhoneValidator>(context);
    return Consumer(
      builder: (context, PhoneValidator validator, child) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: CustText(
              widget.labelText,
              fontWeight: FontWeight.w400,
            ),
          ),
          SizedBox(height: 10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Flexible(
                flex: 2,
                child: Stack(
                  children: [
                    CustTextFormField(
                      focusNode: widget.areaFocusNode,
                      keyboardType: TextInputType.phone,
                      controller: widget.areaCodeController,
                      decoration: InputDecoration(counterText: ''),
                      maxLength: 3,
                      onChanged: (value) {
                        validator.updateArea(value);
                      },
                    ),
                    Positioned(
                      top: 0,
                      right: 0,
                      bottom: 0,
                      child: IgnorePointer(
                        child: Icon(Icons.arrow_drop_down, size: 20.0, color: CustColor.primaryTextColor),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 20),
              Flexible(
                flex: 5,
                child: CustTextFormField(
                  focusNode: widget.phoneFocusNode,
                  keyboardType: TextInputType.phone,
                  controller: widget.phoneController,
                  onChanged: (value) {
                    validator.updatePhone(value);
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  ListTile _getListTile(String text, String desc) {
    return ListTile(
      dense: true,
      title: Text('$text ($desc)'),
      onTap: () {
        widget.areaCodeController.text = text;
        widget.areaCodeController.selection =
            TextSelection.fromPosition(TextPosition(offset: widget.areaCodeController.text.length));

        widget.areaFocusNode.unfocus();
      },
    );
  }
}

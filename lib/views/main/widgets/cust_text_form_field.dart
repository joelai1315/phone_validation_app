import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:phone_validate_app/constants/cust_color.dart';
import 'package:phone_validate_app/utils/form_utils.dart';

class CustTextFormField extends StatelessWidget {
  final String name;
  final String labelText;
  final InputDecoration decoration;
  final bool readOnly;
  final String initialValue;
  final List<FormFieldValidator<String>> validators;
  final Function(String) onChanged;
  final TextInputType keyboardType;
  final int maxLength;
  final TextEditingController controller;
  final FocusNode focusNode;

  const CustTextFormField({
    Key key,
    this.name,
    this.labelText,
    this.decoration,
    this.readOnly = false,
    this.initialValue,
    this.validators,
    this.onChanged,
    this.keyboardType,
    this.maxLength,
    this.controller,
    this.focusNode,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
      key: ValueKey(name),
      focusNode: focusNode,
      readOnly: readOnly,
      initialValue: initialValue,
      name: name,
      maxLengthEnforcement:
          !readOnly && maxLength != null ? MaxLengthEnforcement.enforced : MaxLengthEnforcement.none,
      maxLength: !readOnly ? maxLength : null,
      keyboardType: keyboardType ?? TextInputType.text,
      decoration: (decoration ?? FormUtils.decoration).copyWith(
        // labelText: labelText,
        enabledBorder:
            !readOnly ? UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey)) : InputBorder.none,
        focusedBorder: !readOnly
            ? UnderlineInputBorder(borderSide: BorderSide(color: CustColor.primaryColor))
            : InputBorder.none,
        disabledBorder: readOnly ? InputBorder.none : null,
        // contentPadding: EdgeInsets.symmetric(vertical: 11),
      ),
      onChanged: (value) {
        if (onChanged != null) onChanged(value);
      },
      maxLines: 1,
      validator: FormBuilderValidators.compose<String>(validators ?? []),
      controller: controller,
    );
  }
}

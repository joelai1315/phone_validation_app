import 'package:flutter/widgets.dart';

class BaseModel extends ChangeNotifier {
  bool hasMounted = true;
  BuildContext context;

  BaseModel(BuildContext context) {
    this.context = context;
  }

  @override
  void dispose() {
    super.dispose();
    hasMounted = false;
  }

  @override
  void notifyListeners() {
    if (hasMounted) super.notifyListeners();
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:phone_validate_app/constants/cust_color.dart';
import 'package:phone_validate_app/services/navigation_service.dart';
import 'package:phone_validate_app/view_models/global_view_model.dart';
import 'package:provider/provider.dart';

abstract class BaseScreen<T> extends StatefulWidget {
  final T params;
  BaseScreen(this.params, {Key key}) : super(key: key);
}

abstract class BaseState<Page extends BaseScreen> extends State<Page> with WidgetsBindingObserver {
  Color backgroundColor() => CustColor.backgroundColor;
  String screenName();
  Widget body();
  Widget bigBody() => body();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (NavigationService.instance.getRouteStack().indexOf(screenName()) > 0) {
          return true;
        } else {
          // TODO: Quit App
        }
        return false;
      },
      child: Consumer(
        builder: (context, Global model, _) => ExcludeSemantics(
          child: bodyByWidth(),
        ),
      ),
    );
  }

  bodyByWidth() {
    var smallestDimension = MediaQuery.of(context).size.shortestSide;
    final useMobileLayout = smallestDimension < 600;
    if (!useMobileLayout) {
      return bigBody();
    } else {
      return body();
    }
  }
}

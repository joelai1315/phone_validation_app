import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:phone_validate_app/routes/route_names.dart';
import 'package:phone_validate_app/routes/router.dart';
import 'package:phone_validate_app/services/storage_service.dart';
import 'package:phone_validate_app/utils/translate_preferences.dart';
import 'package:phone_validate_app/view_models/global_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

void initApp() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light
      .copyWith(statusBarIconBrightness: Brightness.light, statusBarBrightness: Brightness.light));
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  var delegate = await LocalizationDelegate.create(
      preferences: TranslatePreferences(),
      fallbackLocale: Language.fromInt(Language.english).toString(),
      supportedLocales: [
        Language.fromInt(Language.english).toString(),
        Language.fromInt(Language.tchinese).toString(),
        Language.fromInt(Language.schinese).toString(),
      ]);
  WidgetsFlutterBinding.ensureInitialized();
  Global.instance.updateTheme(await StorageService.getInt(StorageKeys.theme));
  runApp(LocalizedApp(delegate, App()));
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var localizationDelegate = LocalizedApp.of(context).delegate;
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => Global.instance),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        localizationsDelegates: [
          GlobalCupertinoLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          localizationDelegate
        ],
        supportedLocales: localizationDelegate.supportedLocales,
        locale: localizationDelegate.currentLocale,
        onGenerateRoute: generateRoute,
        initialRoute: RouteName.SplashRoute,
        theme: ThemeData(textTheme: GoogleFonts.nunitoSansTextTheme()),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_translate/global.dart';
import 'package:phone_validate_app/constants/cust_font_size.dart';
import 'package:phone_validate_app/generated/Localized.dart';
import 'package:phone_validate_app/routes/route_names.dart';
import 'package:phone_validate_app/view_models/global_view_model.dart';
import 'package:phone_validate_app/widgets/cust_app_bar.dart';
import 'package:phone_validate_app/widgets/cust_text.dart';

import '../base/base_screen.dart';

class ResultViewParam {}

class ResultView extends BaseScreen<ResultViewParam> {
  ResultView(ResultViewParam params) : super(params);

  @override
  _ResultViewState createState() => _ResultViewState();
}

class _ResultViewState extends BaseState<ResultView> {
  @override
  Widget body() {
    var items = Global.instance.phoneRecords.reversed.toList();

    return Scaffold(
      appBar: CustAppBar(context: context, screenName: screenName(), title: translate(Localized.Record)),
      backgroundColor: backgroundColor(),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(20),
              child: CustText(
                translate(Localized.Your_Phone_Is_Valid),
                size: CustFontSize.xxLarge,
                fontWeight: FontWeight.bold,
              ),
            ),
            Divider(thickness: 5),
            Padding(
              padding: EdgeInsets.all(10),
              child: CustText(
                translate(Localized.History),
              ),
            ),
            Expanded(
              child: ListView.separated(
                itemBuilder: (context, index) => ListTile(title: CustText(items[index])),
                itemCount: items.length,
                separatorBuilder: (context, index) => Divider(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  String screenName() => RouteName.ResultRoute;
}

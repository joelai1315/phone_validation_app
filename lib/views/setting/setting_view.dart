import 'package:flutter/material.dart';
import 'package:flutter_translate/global.dart';
import 'package:phone_validate_app/constants/cust_color.dart';
import 'package:phone_validate_app/constants/cust_font_size.dart';
import 'package:phone_validate_app/generated/Localized.dart';
import 'package:phone_validate_app/routes/route_names.dart';
import 'package:phone_validate_app/view_models/global_view_model.dart';
import 'package:phone_validate_app/views/setting/widgets/theme_select.dart';
import 'package:phone_validate_app/widgets/cust_app_bar.dart';
import 'package:phone_validate_app/widgets/cust_text.dart';
import 'package:phone_validate_app/views/setting/widgets/lang_select.dart';

import '../base/base_screen.dart';

class SettingViewParam {}

class SettingView extends BaseScreen<SettingViewParam> {
  SettingView(SettingViewParam params) : super(params);

  @override
  _SettingViewState createState() => _SettingViewState();
}

class _SettingViewState extends BaseState<SettingView> {
  @override
  Widget body() {
    return Scaffold(
      appBar: CustAppBar(context: context, screenName: screenName(), title: translate(Localized.Record)),
      backgroundColor: backgroundColor(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(15, 15, 15, 10),
            child: CustText(
              translate(Localized.Language),
              size: CustFontSize.large,
            ),
          ),
          LangSelect(
            fontColor: CustColor.primaryTextColor,
            onChange: (value) {
              Global.instance.currentLang = value;
              setState(() {});
            },
          ),
          Divider(thickness: 5),
          Padding(
            padding: EdgeInsets.fromLTRB(15, 15, 15, 10),
            child: CustText(
              translate(Localized.Theme),
              size: CustFontSize.large,
            ),
          ),
          ThemeSelect(
            onChange: (value) {
              Global.instance.updateTheme(value);
              // setState(() {});
            },
          ),
        ],
      ),
    );
  }

  @override
  String screenName() => RouteName.SettingRoute;
}

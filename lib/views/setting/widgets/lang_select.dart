import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:phone_validate_app/constants/cust_color.dart';
import 'package:phone_validate_app/constants/cust_font_size.dart';
import 'package:phone_validate_app/generated/Localized.dart';
import 'package:phone_validate_app/view_models/global_view_model.dart';
import 'package:phone_validate_app/utils/translate_preferences.dart';
import 'package:phone_validate_app/widgets/cust_button.dart';

class LangSelect extends StatelessWidget {
  final Function(int) onChange;
  final Color fontColor;
  const LangSelect({Key key, this.onChange, this.fontColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: Row(
        children: <Widget>[
          // if (Global.instance.currentLang != Language.english)
          SizedBox(
            width: 100,
            child: CustButton(translate(Localized.Lang_Short_En), padding: EdgeInsets.zero, onTap: (_) async {
              await changeLocale(context, Language.fromInt(Language.english).toString());
              if (onChange != null) onChange(Language.english);
            },
                bgColor: Colors.transparent,
                fontColor:
                    Global.instance.currentLang == Language.english ? CustColor.primaryColor : this.fontColor,
                borderColor: Colors.transparent,
                fontSize: CustFontSize.medium),
          ),
          SizedBox(
            width: 100,
            child: CustButton(translate(Localized.Lang_Short_Tc), padding: EdgeInsets.zero, onTap: (_) async {
              await changeLocale(context, Language.fromInt(Language.tchinese).toString());
              if (onChange != null) onChange(Language.tchinese);
            },
                bgColor: Colors.transparent,
                fontColor: Global.instance.currentLang == Language.tchinese
                    ? CustColor.primaryColor
                    : this.fontColor,
                borderColor: Colors.transparent,
                fontSize: CustFontSize.medium),
          ),
          SizedBox(
            width: 100,
            child: CustButton(translate(Localized.Lang_Short_Sc), padding: EdgeInsets.zero, onTap: (_) async {
              await changeLocale(context, Language.fromInt(Language.schinese).toString());
              if (onChange != null) onChange(Language.schinese);
            },
                bgColor: Colors.transparent,
                fontColor: Global.instance.currentLang == Language.schinese
                    ? CustColor.primaryColor
                    : this.fontColor,
                borderColor: Colors.transparent,
                fontSize: CustFontSize.medium),
          ),
        ],
      ),
    );
  }
}

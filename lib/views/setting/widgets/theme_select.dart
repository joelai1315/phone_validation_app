import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:phone_validate_app/constants/cust_color.dart';
import 'package:phone_validate_app/constants/cust_font_size.dart';
import 'package:phone_validate_app/generated/Localized.dart';
import 'package:phone_validate_app/view_models/global_view_model.dart';
import 'package:phone_validate_app/utils/translate_preferences.dart';
import 'package:phone_validate_app/widgets/cust_button.dart';

class ThemeSelect extends StatelessWidget {
  final Function(int) onChange;
  const ThemeSelect({
    Key key,
    this.onChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 100,
            child: CustButton(
              'Orange',
              padding: EdgeInsets.zero,
              onTap: (_) async {
                onChange(0);
              },
              bgColor: Colors.transparent,
              fontColor: Global.instance.theme == 0 ? CustColor.primaryColor : CustColor.primaryTextColor,
              borderColor: Colors.transparent,
              fontSize: CustFontSize.medium,
            ),
          ),
          SizedBox(
            width: 100,
            child: CustButton(
              'Blue',
              padding: EdgeInsets.zero,
              onTap: (_) async {
                onChange(1);
              },
              bgColor: Colors.transparent,
              fontColor: Global.instance.theme == 1 ? CustColor.primaryColor : CustColor.primaryTextColor,
              borderColor: Colors.transparent,
              fontSize: CustFontSize.medium,
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:phone_validate_app/constants/cust_color.dart';
import 'package:phone_validate_app/constants/cust_font_size.dart';
import 'package:phone_validate_app/generated/image_assets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:phone_validate_app/routes/route_names.dart';
import 'package:phone_validate_app/services/navigation_service.dart';
import 'package:phone_validate_app/widgets/cust_text.dart';

class SplashView extends StatefulWidget {
  @override
  _SplashViewState createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(seconds: 2), () {
      NavigationService.instance.pushNamed(context, RouteName.MainRoute);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: CustColor.backgroundColor,
      child: Center(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          CustText(
            env['WELCOME'],
            size: CustFontSize.xSmall,
          ),
          SizedBox(height: 20),
          Image.asset(
            ImageAssets.phoneUvalidation,
            fit: BoxFit.fitWidth,
          ),
        ],
      )),
    );
  }
}

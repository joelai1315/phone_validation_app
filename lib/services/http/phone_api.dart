import 'package:flutter/material.dart';
import 'package:phone_validate_app/models/region_info.dart';

import 'base_http_client.dart';

class PhoneApi extends BaseHttpClient {
  Future<List<RegionInfo>> getRegions() {
    var path = 'http://country.io/phone.json';
    return request(null, path).then((res) {
      List<RegionInfo> ret = [];
      if (res is Map) {
        for (var key in res.keys) {
          ret.add(RegionInfo(code: key, prefix: res[key]));
        }
      }
      return ret;
    }).catchError((err) {
      debugPrint(err.toString());
      return null;
    });
  }
}

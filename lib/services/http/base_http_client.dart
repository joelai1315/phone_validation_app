import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:phone_validate_app/utils/translate_preferences.dart';
import 'package:phone_validate_app/view_models/global_view_model.dart';

abstract class BaseHttpClient {
  Dio dio;

  BaseHttpClient() {
    dio = Dio(BaseOptions(
      connectTimeout: 20 * 1000,
      receiveTimeout: 60 * 1000,
    ));
  }

  Future<T> request<T>(
    BuildContext context,
    String path, {
    String method = 'GET',
    Map<String, dynamic> header,
    Map<String, dynamic> query,
    Map<String, dynamic> body,
    bool skipAuthHandling = false,
    bool skipErrorPrompt = false,
  }) async {
    try {
      Map<String, dynamic> combinedHeader = (header ?? {})..addAll(commonHeaders);
      Map<String, dynamic> combinedQuery = (query ?? {})..addAll(commonQueries);
      Map<String, dynamic> combinedBody = (body ?? {})..addAll(commonParams);

      // debugPrint('Request: $method $path');
      // LoadingHelper.instance?.add(pageTag);
      Response response = await dio.request(path,
          queryParameters: combinedQuery,
          data: combinedBody,
          options: Options(
            headers: combinedHeader,
            method: method,
          ));

      // LoadingHelper.instance?.reduce(pageTag);
      if (_isSuccess(response.statusCode)) {
        return response.data;
      }
      return null;
    } catch (error, _) {
      // LoadingHelper.instance?.reduce(pageTag);
      rethrow;
    }
  }

  Map<String, dynamic> get commonHeaders {
    var headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };
    return headers;
  }

  Map<String, dynamic> get commonQueries {
    var params = {'language': Language.fromInt(Global.instance.currentLang).toCode()};
    return params;
  }

  Map<String, dynamic> get commonParams {
    var params = {'language': Language.fromInt(Global.instance.currentLang).toCode()};
    return params;
  }

  bool _isSuccess(int statusCode) {
    return statusCode >= 200 && statusCode < 300;
  }
}

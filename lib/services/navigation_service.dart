import 'package:flutter/material.dart';
import 'package:phone_validate_app/routes/route_names.dart';

class NavigationService {
  static NavigationService instance = NavigationService();

  List<String> _routeStack = [];

  List<String> getRouteStack() {
    return _routeStack;
  }

  bool isLastRoute(String name) {
    return getRouteStack() != null && getRouteStack().isNotEmpty && getRouteStack().last == name;
  }

  Future<dynamic> pushNamed(context, String routeName, {Object arguments}) {
    _routeStack.add(routeName);
    return Navigator.of(context).pushNamed(routeName, arguments: arguments);
  }

  Future<dynamic> pushReplacementNamed(context, String routeName, {Object arguments}) {
    _routeStack.add(routeName);
    return Navigator.of(context).pushReplacementNamed(
      routeName,
      arguments: arguments,
    );
  }

  Future<dynamic> popUntilAndPushNamed(
    context,
    String untilRouteName,
    String routeName, {
    Object arguments,
  }) {
    if (_routeStack.isNotEmpty) {
      while (_routeStack.isNotEmpty) {
        if (_routeStack.last != untilRouteName) {
          _routeStack.removeLast();
        } else {
          break;
        }
      }
    }
    _routeStack.add(routeName);
    return Navigator.of(context).pushNamedAndRemoveUntil(
      routeName,
      ModalRoute.withName(untilRouteName),
      arguments: arguments,
    );
  }

  Future<dynamic> popAllAndPushNamed(
    context,
    String routeName, {
    Object arguments,
  }) {
    _routeStack.clear();
    _routeStack.add(routeName);
    return Navigator.of(context).pushNamedAndRemoveUntil(
      routeName,
      ModalRoute.withName(RouteName.SplashRoute),
      arguments: arguments,
    );
  }

  void pop(context, {String routeName, dynamic returnValue}) {
    if (_routeStack.isNotEmpty && (routeName == null || _routeStack.last == routeName)) {
      _routeStack.removeLast();
      return Navigator.of(context).pop(returnValue);
    }
  }

  void popUntil(context, String routeName) {
    if (_routeStack.isNotEmpty) {
      while (_routeStack.isNotEmpty) {
        if (_routeStack.last != routeName) {
          _routeStack.removeLast();
        } else {
          break;
        }
      }
    }
    return Navigator.of(context).popUntil(ModalRoute.withName(routeName));
  }

  void popAndPushNamed(
    context,
    String routeName, {
    Object arguments,
  }) {
    if (_routeStack.isNotEmpty) {
      _routeStack.removeLast();
    }
    _routeStack.add(routeName);
    Navigator.of(context).popAndPushNamed(routeName, arguments: arguments);
  }
}

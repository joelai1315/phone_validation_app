import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:phone_validate_app/constants/cust_color.dart';
import 'package:phone_validate_app/generated/Localized.dart';

class FormUtils {
  static InputDecoration decoration = InputDecoration(
    helperStyle: TextStyle(color: Colors.grey[300]),
    labelStyle: TextStyle(color: Colors.grey, height: 0),
    enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
    focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: CustColor.primaryColor)),
    errorMaxLines: 5,
    contentPadding: EdgeInsets.symmetric(vertical: 10),
  );

  static requiredValidator(context) =>
      FormBuilderValidators.required(context, errorText: translate(Localized.Field_Required));
}

import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:devicelocale/devicelocale.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:intl/intl.dart';
import 'package:phone_validate_app/view_models/global_view_model.dart';
import 'package:phone_validate_app/services/storage_service.dart';

class Language {
  static const int english = 0;
  static const int tchinese = 1;
  static const int schinese = 2;

  int _value;

  @override
  toString() {
    switch (_value) {
      case Language.english:
        return 'en';
      case Language.tchinese:
        return 'zh_Hant';
      case Language.schinese:
        return 'zh_Hans';
      default:
        return '';
    }
  }

  toCode() {
    switch (_value) {
      case Language.english:
        return 'en';
      case Language.tchinese:
        return 'zh';
      case Language.schinese:
        return 'cn';
      default:
        return '';
    }
  }

  static Language fromInt(int value) {
    Language lang = Language();
    lang._value = value;
    return lang;
  }

  static Language fromString(String value) {
    Language lang = Language();
    switch (value) {
      case 'en':
        lang._value = Language.english;
        break;
      case 'zh_Hant':
        lang._value = Language.tchinese;
        break;
      case 'zh_Hans':
        lang._value = Language.schinese;
        break;
    }
    return lang;
  }
}

class TranslatePreferences implements ITranslatePreferences {
  @override
  Future<Locale> getPreferredLocale() async {
    final locale = await StorageService.getString(StorageKeys.lang);
    if (locale != null) {
      if (int.tryParse(locale) != null) {
        Global.instance.currentLang = int.parse(locale);

        Intl.defaultLocale =
            localeFromString(Language.fromInt(Global.instance.currentLang).toString()).languageCode;
        return localeFromString(Language.fromInt(Global.instance.currentLang).toString());
      } else {
        StorageService.removeValue(StorageKeys.lang);
      }
    }
    var locales = await Devicelocale.preferredLanguages;
    Language lang = Language.fromInt(Language.english);
    if (locales.first.toString().toLowerCase().contains('hant')) {
      lang = Language.fromInt(Language.tchinese);
    } else if (locales.first.toString().toLowerCase().contains('hans')) {
      lang = Language.fromInt(Language.schinese);
    }
    Global.instance.currentLang = lang._value;
    Intl.defaultLocale = Locale(lang.toString()).languageCode;
    return Locale(lang.toString());
  }

  @override
  Future savePreferredLocale(Locale locale) async {
    Global.instance.currentLang = Language.fromString(locale.toString())._value;
    Intl.defaultLocale =
        localeFromString(Language.fromInt(Global.instance.currentLang).toString()).languageCode;
    await StorageService.setString(StorageKeys.lang, Global.instance.currentLang.toString());
  }
}

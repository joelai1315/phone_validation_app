import 'package:flutter_translate_annotations/flutter_translate_annotations.dart';

part 'Localized.g.dart';

@TranslateKeysOptions(path: 'assets/i18n', caseStyle: CaseStyle.titleCase, separator: '_')
class _$Localized // ignore: unused_element
{}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Localized.dart';

// **************************************************************************
// Generator: FlutterTranslateGen
// **************************************************************************

class Localized {
  static const String Error = 'error';

  static const String Home = 'home';

  static const String Invalid_Format = 'invalid.format';

  static const String Submit = 'submit';

  static const String Ok = 'ok';

  static const String Field_Required = 'field.required';

  static const String Phone_No = 'phone.no';

  static const String Lang_Short_En = 'lang.short.en';

  static const String Lang_Short_Tc = 'lang.short.tc';

  static const String Lang_Short_Sc = 'lang.short.sc';

  static const String Loading = 'loading';

  static const String Record = 'record';

  static const String Validate_Phone = 'validate.phone';

  static const String Your_Phone_Is_Valid = 'your.phone.is.valid';

  static const String History = 'history';

  static const String Setting = 'setting';

  static const String Language = 'Language';

  static const String Theme = 'Theme';

  static const String Light = 'Light';

  static const String Dark = 'Dark';
}

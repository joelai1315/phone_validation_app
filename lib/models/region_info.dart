class RegionInfo {
  String code;
  String prefix;

  RegionInfo({this.code, this.prefix});

  RegionInfo.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    prefix = json['prefix'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['code'] = this.code;
    data['prefix'] = this.prefix;
    return data;
  }
}

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:phone_validate_app/views/app.dart';

void main() async {
  await load(fileName: 'assets/env/prod.env');
  initApp();
}

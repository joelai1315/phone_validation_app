import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:phone_validate_app/views/main/main_view.dart';
import 'package:phone_validate_app/views/result/result_view.dart';
import 'package:phone_validate_app/views/setting/setting_view.dart';
import 'package:phone_validate_app/views/splash_view.dart';
import 'route_names.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case RouteName.SplashRoute:
      return _getPageRoute(settings, SplashView(), PageTransitionType.fadeIn);
    case RouteName.MainRoute:
      return _getPageRoute(settings, MainView(settings.arguments), PageTransitionType.fadeIn);
    case RouteName.ResultRoute:
      return _getPageRoute(settings, ResultView(settings.arguments), PageTransitionType.slideInLeft);
    case RouteName.SettingRoute:
      return _getPageRoute(settings, SettingView(settings.arguments), PageTransitionType.slideInLeft);
    default:
      return _getPageRoute(settings, Container(), PageTransitionType.fadeIn);
  }
}

PageRoute _getPageRoute(RouteSettings settings, Widget child, PageTransitionType type) {
  return PageRouteBuilder<dynamic>(
      settings: settings,
      pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
        return child;
      },
      transitionDuration: const Duration(milliseconds: 300),
      transitionsBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation, Widget child) {
        return effectMap[type](Curves.ease, animation, secondaryAnimation, child);
      });
}

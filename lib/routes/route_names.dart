abstract class RouteName {
  static const SplashRoute = '/Splash';
  static const MainRoute = '/main';
  static const ResultRoute = '/result';
  static const SettingRoute = '/setting';
}

import 'dart:ui';
import 'package:flutter/material.dart';

class CustColor {
  static Color primaryColor = Colors.deepOrange;
  static Color appBarColor = Colors.white;
  static Color backgroundColor = Colors.white;
  static Color primaryTextColor = Colors.black;
  static Color primaryOppositeTextColor = Colors.white;

  static updateColorByTheme(int theme) {
    switch (theme) {
      case 0:
        CustColor.primaryColor = Colors.deepOrange;
        CustColor.appBarColor = Colors.white;
        CustColor.backgroundColor = Colors.white;
        CustColor.primaryTextColor = Colors.black;
        CustColor.primaryOppositeTextColor = Colors.white;
        break;
      case 1:
        CustColor.primaryColor = Colors.blue;
        CustColor.appBarColor = Colors.white;
        CustColor.backgroundColor = Colors.white;
        CustColor.primaryTextColor = Colors.black;
        CustColor.primaryOppositeTextColor = Colors.white;
        break;
      default:
    }
  }
}

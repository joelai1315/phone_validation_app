class CustFontSize {
  static const double xxxxSmall = 9.0;
  static const double xxxSmall = 10.0;
  static const double xxSmall = 11.0;
  static const double xSmall = 12.0;
  static const double small = 14.0;
  static const double medium = 16.0;
  static const double large = 18.0;
  static const double xLarge = 20.0;
  static const double xxLarge = 24.0;
  static const double xxxLarge = 28.0;
  static const double xxxxLarge = 32.0;
  static const double xxxxxLarge = 36.0;
  static const double xxxxxxLarge = 40.0;
}
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:phone_validate_app/constants/cust_color.dart';

void main() {
  test(
      'GIVEN theme is light '
      'THEN primary color is deepOrange', () async {
    CustColor.updateColorByTheme(0);
    expect(CustColor.primaryColor, Colors.deepOrange);
  });

  test(
      'GIVEN theme is drak '
      'THEN primary color is not deepOrange', () async {
    CustColor.updateColorByTheme(1);
    expect(CustColor.primaryColor, isNot(Colors.deepOrange));
  });
}

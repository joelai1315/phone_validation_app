# Phone Validate App

## ENV

launch flutter app with param "-t main.dart" or "-t main_prod.dart" for respective environment

## Pages

### Splash View

**contains**

1. a image

**remarks**:

1. show for 2 seconds then navigate to next screen

### Main View

**contains**

1. a phone field with a dropdown textfield for areacode input and simple textfield for phone input
1. a button for trigger validation, if valid, navigate to record screen; if fail, prompt error
1. a action button on top right corner for navigating to setting page

**remarks**:

1. For Phone Width, the button is at the bottom; For Tablet Width, the button is at the right
1. Base on device locale country code, will prefil it's respective area code 

### Record View

**contains**

1. a tableview showing all previous valid phones within a lifetime of app


### Setting View

**contains**

1. a language select button
2. a theme select button

## Testing

 phone\_validate\_test.dart
 
 1. Check if changing theme will update the color
